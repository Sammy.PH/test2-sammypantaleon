//Sammy Pantaleon-Hernandez
package question3and4;
import java.util.*;
public class CollectionMethods {
	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
			for(Planet x : planets) {
				if(x.getOrder() != 1 && x.getOrder() != 2 && x.getOrder() != 3) {
					planets.remove(x);
				}
			}
		return planets;
	}
}
