//Sammy Pantaleon-Hernandez
package question2;

public class HourlyEmployee implements Employee{
	private int weeklyHours;
	private int hourlyPay;
	
	//Constructor
	public HourlyEmployee(int weeklyHours, int hourlyPay) {
		this.weeklyHours = weeklyHours;
		this.hourlyPay = hourlyPay;
	}
	
	public int getYearlyPay() {
		return (this.hourlyPay * this.weeklyHours * 52);
	}
}
