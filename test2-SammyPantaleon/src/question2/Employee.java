//Sammy Pantaleon-Hernandez
package question2;

public interface Employee {
	int getYearlyPay();
}
