//Sammy Pantaleon-Hernandez
package question2;

public class SalariedEmployee implements Employee{
	private int yearly;
	
	//Constructor
	public SalariedEmployee(int yearly) {
		this.yearly = yearly;
	}
	
	public int getYearlyPay() {
		return this.yearly;
	}
}
