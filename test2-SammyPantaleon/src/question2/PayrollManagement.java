//Sammy Pantaleon-Hernandez
package question2;

public class PayrollManagement {
	public static void main(String[] args) {
		Employee[] allEmployee = new Employee[5];
		allEmployee[0] = new SalariedEmployee(40000);
		allEmployee[1] = new HourlyEmployee(40, 20);
		allEmployee[2] = new UnionizedHourlyEmployee(40, 35, 5000);
		allEmployee[3] = new HourlyEmployee(40, 50);
		allEmployee[4] = new UnionizedHourlyEmployee(40, 15, 1000);
		System.out.println(getTotalExpenses(allEmployee));
	}
	
	public static int getTotalExpenses(Employee[] allEmployee) {
		int total = 0;
		for (Employee x : allEmployee) {
			total = total + x.getYearlyPay();
		}
		return total;
	}
}
