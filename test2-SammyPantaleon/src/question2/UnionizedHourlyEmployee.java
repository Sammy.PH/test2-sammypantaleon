//Sammy Pantaleon-Hernandez
package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int pensionContribution;
		
	//Constructor
	public UnionizedHourlyEmployee(int weeklyHours, int hourlyPay, int pensionContribution) {
		super(weeklyHours, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	
	public int getYearlyPay() {
		int yearlyIncome = super.getYearlyPay() + pensionContribution;
		return yearlyIncome;
	}
}
